<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label >First Name</label><br><br>
        <input name="first_name"type="text"><br><br>
        <label >Last Name</label><br><br>
        <input name="last_name" type="text"><br><br>
        <label >Gender</label><br><br>
        <input type="radio" name="Gender" value="Male">
        <label>Male</label><br>
        <input type="radio" name="Gender" value="Female">
        <label>Female</label><br>
        <input type="radio" name="Gender" value="Other">
        <label>Other</label><br><br>
        <label>Nationality</label><br><br>
        <select name="Nationality" id="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="English">English</option>
            <option value="Other">Other</option>
        </select><br><br>
        <label >Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa" value="English">English<br>
        <input type="checkbox" name="bahasa" value="Arabic">Arabic<br>
        <input type="checkbox" name="bahasa" value="Japanese">Japanese<br>
        <label >Bio</label><br><br>
        <textarea name="Bio"cols="30" rows="10"></textarea><br><br>
        <input type="Submit" value="Sign Up">
    </form>

</body>
</html>