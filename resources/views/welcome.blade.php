<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    @if(isset($first_name))
    <h1>Selamat Datang {{$first_name}} {{$last_name}}</h1>
    <br>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama! </h2>
    @else
    <br><br>
    <h1 align="center">Register terlebih dahulu <a href="/register">Klik Disini</a> </h1>
    @endif
</body>
</html>